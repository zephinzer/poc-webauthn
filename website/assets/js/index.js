(function() {
  if (!window.PublicKeyCredential) {
    alert("This POC might not work as expected because window.PublicKeyCredential is not available");
  }
})();

function login() {
  console.log("login(): triggered");
  var username = document.getElementById("username").value;
  var request = new XMLHttpRequest();
  request.addEventListener("loadstart", function(e) {
    console.log("login(): starting request with username '" + username + "'...", e);
  });
  request.addEventListener("load", function(e) {
    console.log("login(): request complete, response follows", e);
    var response = JSON.parse(request.response);
    alert("response from server: " + request.response);
    console.log("response:  ", response);

    response.publicKey.challenge = decode(response.publicKey.challenge);
    for (var i = 0; i < response.publicKey.allowCredentials.length; i++) {
      response.publicKey.allowCredentials[i].id = decode(response.publicKey.allowCredentials[i].id)
    }
    return navigator.credentials.get({
      publicKey: response.publicKey,
    }).then((function(assertion) {
      request = new XMLHttpRequest();
      request.addEventListener("loadstart", function(e) {
        console.log("login(): sending assertion back...", e);
      });
      request.addEventListener("load", function(e) {
        var response = JSON.parse(request.response);
        console.log("login(): response:  ", response);
        if (response === "ok") {
          document.getElementById("login-success").classList.remove("hidden");
          setTimeout(function() {
            document.getElementById("login-success").classList.add("hidden");
          }, 3000)
        }
      })
      request.open("POST", "/login/" + username);
      var bodyData = {
        id: assertion.id,
        rawId: encode(assertion.rawId),
        type: assertion.type,
        response: {
          authenticatorData: encode(assertion.response.authenticatorData),
          clientDataJSON: encode(assertion.response.clientDataJSON),
          signature: encode(assertion.response.signature),
          userHandle: encode(assertion.response.userHandle),
        },
      };
      var body = JSON.stringify(bodyData);
      alert("login(): sending following data to server: " + body);
      console.log("login(): sending following data to server:", body);
      request.send(body);
    })).catch(function(err) {
      // this alert helps to debug this shit on mobile
      if (err) {
        alert("login(): errored: ", err);
        console.log("login(): errored: ", err);
        document.getElementById("login-failed").classList.remove("hidden");
        setTimeout(function() {
          document.getElementById("login-failed").classList.add("hidden");
        }, 3000)
      }
    });
  });
  request.open("GET", "/login/" + username);
  request.send();
}

function register() {
  console.log("register(): triggered");
  var username = document.getElementById("username").value;
  var request = new XMLHttpRequest();
  request.addEventListener("loadstart", function(e) {
    console.log("register(): starting request with username '" + username + "'...", e);
  });
  request.addEventListener("load", function(e) {
    console.log("register(): request complete, response follows", e);
    var response = JSON.parse(request.response);
    console.log("response:  ", response);

    // this seemingly redundant transformation seems required to adhere to the interface
    response.publicKey.challenge = decode(response.publicKey.challenge);
    response.publicKey.user.id = decode(response.publicKey.user.id);
    console.log("challenge: ", response.publicKey.challenge);
    console.log("user id  : ", response.publicKey.user.id);

    // this credentials creation is handled by the browser
    return navigator.credentials.create({
      publicKey: response.publicKey,
    }).then(function(credential) {
      console.log(credential);
      var attestationObject = encode(credential.response.attestationObject);
      var clientDataJSON = encode(credential.response.clientDataJSON);
      var rawId = encode(credential.rawId);

      request = new XMLHttpRequest();
      request.addEventListener("loadstart", function(e) {
        console.log("register(): sending credentials back...", e);
      });
      request.addEventListener("load", function(e) {
        var response = JSON.parse(request.response);
        console.log("response:  ", response);
        if (response === "ok") {
          document.getElementById("registration-success").classList.remove("hidden");
          setTimeout(function() {
            document.getElementById("registration-success").classList.add("hidden");
          }, 3000)
        }
      })
      request.open("POST", "/register/" + username);
      var bodyData = {
        id: credential.id,
        rawId: rawId,
        type: credential.type,
        response: {
          attestationObject: attestationObject,
          clientDataJSON: clientDataJSON,
        },
      };
      var body = JSON.stringify(bodyData);
      alert("register(): sending following data to server: " + body);
      console.log("register(): sending following data to server:", body);
      request.send(body);
    }).catch(function(err) {
      // this alert helps to debug this shit on mobile
      if (err) {
        alert("register(): errored: ", err);
        console.log("register(): errored: ", err);
        document.getElementById("registration-failed").classList.remove("hidden");
        setTimeout(function() {
          document.getElementById("registration-failed").classList.add("hidden");
        }, 3000)
      }
    });
  });
  request.open("GET", "/register/" + username);
  request.send();
}

function decode(input) {
  return Uint8Array.from(atob(input), function(c) {
    return c.charCodeAt(0);
  });
}

function encode(value) {
  return btoa(String.fromCharCode.apply(null, new Uint8Array(value)))
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=/g, "");;
}
