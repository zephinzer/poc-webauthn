package mockdata

import (
	_ "embed"
	"encoding/json"
	"fmt"

	"github.com/duo-labs/webauthn/webauthn"
)

//go:embed users.json
var userDataSource string
var userData UserData

type UserData struct {
	Data []User `json:"data"`
}

type User struct {
	Username    string `json:"username"`
	Password    string `json:"password"`
	Email       string `json:"email"`
	Credentials []webauthn.Credential
}

func (u User) WebAuthnCredentials() []webauthn.Credential {
	return u.Credentials
}

func (u User) WebAuthnDisplayName() string {
	return u.Username
}

func (u User) WebAuthnIcon() string {
	return ""
}

func (u User) WebAuthnID() []byte {
	return []byte(u.Email)
}

func (u User) WebAuthnName() string {
	return u.Username
}

func init() {
	err := json.Unmarshal([]byte(userDataSource), &userData)
	if err != nil {
		panic(fmt.Errorf("failed to unmarshal user data in users.json: %s", err))
	}
}

func GetUser(username string) *User {
	for _, u := range userData.Data {
		if username == u.Username {
			return &u
		}
	}
	return nil
}

func ListUsers() []User {
	return userData.Data
}

func SetUserCredentials(username string, credentials webauthn.Credential) error {
	done := false
	for i := 0; i < len(userData.Data); i++ {
		u := &userData.Data[i]
		if username == u.Username {
			u.Credentials = append(u.Credentials, credentials)
			done = true
			break
		}
	}
	if !done {
		return fmt.Errorf("failed to set credentials: username '%s' not found", username)
	}
	return nil
}
