package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"poc-webauthn/internal/authentication"
	"poc-webauthn/internal/mockdata"
	"poc-webauthn/internal/sessions"

	"github.com/duo-labs/webauthn/protocol"
	"github.com/duo-labs/webauthn/webauthn"
	"github.com/gorilla/mux"
)

func LoginGetHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("login get handler called")
	vars := mux.Vars(r)
	username := vars["username"]
	if len(username) == 0 {
		message := fmt.Sprintf("failed to find a valid username")
		log.Println(message)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(message))
		return
	}
	user := mockdata.GetUser(username)
	if user == nil {
		message := fmt.Sprintf("failed to find a username for username '%s'", username)
		log.Println(message)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(message))
		return
	}
	if userJSON, err := json.MarshalIndent(user, "", "  "); err == nil {
		log.Printf("selected user data follows:\n%s\n", userJSON)
	}

	webauthnClient, _ := authentication.GetWebAuthnClient()
	allowedCredentials := make([]protocol.CredentialDescriptor, 1)
	allowedCredentials[0] = protocol.CredentialDescriptor{
		CredentialID: user.Credentials[0].ID,
		Type:         protocol.CredentialType("public-key"),
	}

	opts, sessionData, err := webauthnClient.BeginLogin(
		user,
		webauthn.WithAllowedCredentials(allowedCredentials),
		webauthn.WithUserVerification(protocol.VerificationPreferred),
	)
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to handle user login request: %s", err), w)
		return
	}
	sessionStore, err := sessions.GetStore()
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to get session store: %s", err), w)
		return
	}
	if sessionDataJSON, err := json.MarshalIndent(sessionData, "", "  "); err == nil {
		log.Printf("session data for login request follows:\n", string(sessionDataJSON))
	}
	if err := sessionStore.SaveWebauthnSession("login", sessionData, r, w); err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to save session data for login: %s", err), w)
		return
	}
	returnJSON, err := json.MarshalIndent(opts, "", "  ")
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to marshal login options: %s", err), w)
		return
	}
	log.Println("login handler returning")
	w.WriteHeader(http.StatusOK)
	log.Printf("sending the following response to the browser: %s", returnJSON)
	w.Write(returnJSON)
}

func LoginPostHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("login post handler called")
	vars := mux.Vars(r)
	username := vars["username"]
	if len(username) == 0 {
		message := fmt.Sprintf("failed to find a valid username")
		log.Println(message)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(message))
		return
	}
	user := mockdata.GetUser(username)
	if user == nil {
		message := fmt.Sprintf("failed to find a username for username '%s'", username)
		log.Println(message)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(message))
		return
	}
	if userJSON, err := json.MarshalIndent(user, "", "  "); err == nil {
		log.Printf("selected user data follows:\n%s\n", userJSON)
	}
	sessionStore, err := sessions.GetStore()
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to get session store: %s", err), w)
		return
	}
	sessionData, err := sessionStore.GetWebauthnSession("login", r)
	if err != nil {
		handleError(http.StatusBadRequest, fmt.Sprintf("failed to get session info for completing login: %s", err), w)
		return
	}
	webauthnClient, _ := authentication.GetWebAuthnClient()
	credential, err := webauthnClient.FinishLogin(user, sessionData, r)
	if err != nil {
		handleError(http.StatusBadRequest, fmt.Sprintf("failed to complete login: %s", err), w)
		return
	}

	credentialAsJSON, err := json.MarshalIndent(credential, "", "  ")
	if err != nil {
		log.Printf("DEBUG: failed to marshal credentials for user %s: %s", user.Username, err)
	}
	log.Println("following is the stored credentials in the user object: ")
	log.Println(string(credentialAsJSON))

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("\"ok\""))
}
