package handlers

import (
	"encoding/json"
	"log"
	"net/http"
)

func handleError(statusCode int, errorData interface{}, w http.ResponseWriter) {
	log.Println(errorData)
	w.WriteHeader(statusCode)
	stringData, err := json.Marshal(errorData)
	if err != nil {
		log.Printf("error responding: %s", err)
	}
	w.Write(stringData)
}
