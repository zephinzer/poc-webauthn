package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"poc-webauthn/internal/authentication"
	"poc-webauthn/internal/mockdata"
	"poc-webauthn/internal/sessions"

	"github.com/duo-labs/webauthn/protocol"
	"github.com/duo-labs/webauthn/webauthn"
	"github.com/gorilla/mux"
)

type RegistrationPostBody struct {
	ID       string                   `json:"id"`
	RawID    string                   `json:"rawId"`
	Type     string                   `json:"type"`
	Response RegistrationPostResponse `json:"response"`
}

type RegistrationPostResponse struct {
	AttestationObject string `json:"attestationObject"`
	ClientDataJSON    string `json:"clientDataJSON"`
}

func RegistrationPostHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("registration post handler called")
	vars := mux.Vars(r)
	username := vars["username"]
	if len(username) == 0 {
		handleError(http.StatusBadRequest, "failed to find a valid username", w)
		return
	}
	user := mockdata.GetUser(username)
	if user == nil {
		handleError(http.StatusBadRequest, fmt.Sprintf("failed to find a username for username '%s'", username), w)
		return
	}
	sessionStore, err := sessions.GetStore()
	if err != nil {
		handleError(http.StatusBadRequest, fmt.Sprintf("failed to get session store: %s", err), w)
		return
	}
	sessionData, err := sessionStore.GetWebauthnSession("registration", r)
	if err != nil {
		handleError(http.StatusBadRequest, fmt.Sprintf("failed to get session data: %s", err), w)
		return
	}

	// this section displays the returned data from the browser and is just for
	// demonstration/debugging purposes
	var buf bytes.Buffer
	bodyTee := io.TeeReader(r.Body, &buf)
	body, err := io.ReadAll(bodyTee)
	if err != nil {
		log.Printf("failed to read body data: %s", err)
		return
	}
	var bodyData RegistrationPostBody
	if err := json.Unmarshal(body, &bodyData); err != nil {
		log.Printf("failed to unmarshal body data: %s", err)
		return
	}
	var formattedBody interface{}
	if err := json.Unmarshal(body, &formattedBody); err == nil {
		prettyPrintedBody, err := json.MarshalIndent(formattedBody, "", "  ")
		if err == nil {
			log.Println("following was sent by the browser: ")
			log.Println(string(prettyPrintedBody))
		}
	}
	//// warning: probably don't ever use this since this buffer will never be closed, might cause some issues if in production
	r.Body = io.ReadCloser(io.NopCloser(&buf))
	// end of the previously mentioned section

	webauthnClient, _ := authentication.GetWebAuthnClient()
	credential, err := webauthnClient.FinishRegistration(user, sessionData, r)
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to complete registration: %s", err), w)
		return
	}

	if err := mockdata.SetUserCredentials(username, *credential); err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to save user credentials: %s", err), w)
		return
	}

	credentialAsJSON, err := json.MarshalIndent(user.Credentials, "", "  ")
	if err != nil {
		log.Printf("DEBUG: failed to marshal user credentials for user %s: %s", user.Username, err)
	}
	log.Println("following is the stored credentials in the user object: ")
	log.Println(string(credentialAsJSON))

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("\"ok\""))
}

func RegistrationGetHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("registration get handler called")
	vars := mux.Vars(r)
	username := vars["username"]
	if len(username) == 0 {
		handleError(http.StatusBadRequest, "failed to find a valid username", w)
		return
	}
	user := mockdata.GetUser(username)
	if user == nil {
		handleError(http.StatusBadRequest, fmt.Sprintf("failed to find a username for username '%s'", username), w)
		return
	}
	webauthnClient, _ := authentication.GetWebAuthnClient()

	authenticationSelector := protocol.AuthenticatorSelection{
		AuthenticatorAttachment: protocol.AuthenticatorAttachment("platform"),
		RequireResidentKey:      protocol.ResidentKeyUnrequired(),
		UserVerification:        protocol.VerificationRequired,
	}
	conveyencePreference := protocol.ConveyancePreference(protocol.PreferNoAttestation)

	opts, sessionData, err := webauthnClient.BeginRegistration(
		user,
		webauthn.WithAuthenticatorSelection(authenticationSelector),
		webauthn.WithConveyancePreference(conveyencePreference),
	)
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to register user: %s", err), w)
		return
	}
	sessionStore, err := sessions.GetStore()
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to get session store: %s", err), w)
		return
	}
	if sessionDataJSON, err := json.MarshalIndent(sessionData, "", "  "); err == nil {
		log.Printf("session data for registration request follows:\n", string(sessionDataJSON))
	}
	if err := sessionStore.SaveWebauthnSession("registration", sessionData, r, w); err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to save session data for registration: %s", err), w)
		return
	}

	returnJSON, err := json.MarshalIndent(opts, "", "  ")
	if err != nil {
		handleError(http.StatusInternalServerError, fmt.Sprintf("failed to marshal registration options: %s", err), w)
		return
	}

	log.Println("registration handler returning")
	w.WriteHeader(http.StatusOK)
	log.Printf("sending the following response to the browser: %s", string(returnJSON))
	w.Write(returnJSON)
}
