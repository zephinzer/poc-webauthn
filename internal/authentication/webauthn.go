package authentication

import (
	"fmt"

	"github.com/duo-labs/webauthn/webauthn"
)

var WebAuthnClient *webauthn.WebAuthn
var WebAuthnError error

func init() {
	hostname := "058cbaab92b4.ngrok.io"
	WebAuthnClient, WebAuthnError = webauthn.New(&webauthn.Config{
		// this is just a display name, no need to be careful about anything here
		RPDisplayName: "poc-webauthn",
		// this needs to be the same as the hostname this service is accessed via
		RPID:     hostname,
		RPOrigin: "https://" + hostname,
		// because why not
		Debug: true,
	})
}

func GetWebAuthnClient() (*webauthn.WebAuthn, error) {
	if WebAuthnError != nil {
		return nil, fmt.Errorf("failed to create webauthn client: %s", WebAuthnError)
	}
	return WebAuthnClient, nil
}
