package sessions

import (
	"fmt"

	"github.com/duo-labs/webauthn.io/session"
)

var Store *session.Store
var StoreError error

func init() {
	Store, StoreError = session.NewStore()
}

func GetStore() (*session.Store, error) {
	if StoreError != nil {
		return nil, fmt.Errorf("failed to initiailise session store: %s", StoreError)
	}
	return Store, nil
}
