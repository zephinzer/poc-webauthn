package networking

import (
	"fmt"
	"net/http"
)

type ServerOptions struct {
	Interface string
	Port      uint16
}

func GetHTTPServer(opts ServerOptions) *http.Server {
	s := http.Server{
		Addr: fmt.Sprintf("%s:%v", opts.Interface, opts.Port),
	}
	return &s
}

func StartHTTPServer(server *http.Server, handler http.Handler) error {
	server.Handler = handler
	return server.ListenAndServe()
}
