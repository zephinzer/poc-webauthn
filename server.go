// +build server

package main

import (
	"log"
	"poc-webauthn/cmd/server"
)

func init() {
	log.Println("initialising server command...")
	command = server.GetCommand()
}
