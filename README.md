# POC WebAuthn

This is a mini project to test our how WebAuthn can be used.

# Reference Links

- [Duo Labs WebAuthn Go library](https://github.com/duo-labs/webauthn): library used in the POC
- [WebAuthn website by Duo](https://webauthn.io/): demo website for library used in the POC
- [WebAuthn website by Auth0](https://webauthn.me/): walkthrough of the WebAuthn protocol in general
- [WebAuthn Basic Web Client/Server on Herbie's blog](WebAuthn Basic Web Client/Server): a userful starting article

# Project Structure

This project kinda-sorta follows the [Standard Go Project Layout](https://github.com/golang-standards/project-layout)

# Usage

This project is intended for usage as a learning tool for WebAuthn.

## Getting Started

> You can use [Localtunnel](https://github.com/localtunnel/localtunnel) or [Ngrok](https://ngrok.com/) for this.

- To get started, create a tunnel to port 1234 on your local machine.
- Note the provided public URL and copy the hostname (**this means without the url scheme (eg. http://, https://)**)
- Go to [`./internal/authentication/webauthn.go`](./internal/authentication/webauthn.go) and paste this hostname into the `RPID` value of the configuration
- Access your deployment using the HTTPS version of the public URL (WebAuthn only works on HTTPS, security details are out of scope for this getting started section but it should be trivial to understand why)
