run-server: server
	./bin/server

server:
	@$(MAKE) build name=server

build:
	mkdir -p ./bin
	go build \
		-tags ${name} \
		-o ./bin/${name} \
		.
