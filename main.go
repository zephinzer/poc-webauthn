package main

import (
	"log"
	"os"

	"github.com/spf13/cobra"
)

var command *cobra.Command

func main() {
	if err := command.Execute(); err != nil {
		log.Fatalf("failed to execute command: %s", err)
		os.Exit(1)
	}
}
