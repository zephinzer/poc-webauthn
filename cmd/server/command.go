package server

import (
	"fmt"
	"net/http"
	"poc-webauthn/internal/handlers"
	"poc-webauthn/internal/networking"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use: "server",
		RunE: func(_ *cobra.Command, _ []string) error {
			serverInstance := networking.GetHTTPServer(networking.ServerOptions{
				Interface: "0.0.0.0",
				Port:      1234,
			})
			handler := mux.NewRouter()
			// handles the registration request for server data
			handler.HandleFunc("/register/{username}", handlers.RegistrationGetHandler).Methods("GET")

			// handles the registration callback with client data
			handler.HandleFunc("/register/{username}", handlers.RegistrationPostHandler).Methods("POST")

			// handles the login request
			handler.HandleFunc("/login/{username}", handlers.LoginGetHandler).Methods("GET")
			handler.HandleFunc("/login/{username}", handlers.LoginPostHandler).Methods("POST")
			handler.PathPrefix("/").Handler(http.FileServer(http.Dir("website")))
			networking.StartHTTPServer(serverInstance, handler)
			if err := serverInstance.ListenAndServe(); err != nil {
				return fmt.Errorf("failed to start the server instance: %s", err)
			}
			return nil
		},
	}
	return &command
}
